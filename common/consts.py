DEFAULT_MQTT_CLIENT_ID = "DahuaVTO2MQTT"
DEFAULT_MQTT_TOPIC_PREFIX = "DahuaVTO"

VERSION_FILE = "data/version.json"
CONFIG_FILE = "data/config.json"

UNOFFICIAL_VERSION = "unofficial"

DEFAULT_EXPORTER_PORT = 9563

PROTOCOLS = {
    True: "https",
    False: "http"
}

DAHUA_DEVICE_TYPE = "deviceType"
DAHUA_SERIAL_NUMBER = "serialNumber"

DEFAULT_MQTT_ERROR_MESSAGE_ID = 13

TOPIC_COMMAND = "/Command"

MAX_MESSAGES_IN_BULK = 10

PLACE_HOLDERS = ["{\"id\"", "{\"error\""]
UNICODE_APOSTROPHES = ["“", "”"]

CONCAT_ACTION_MESSAGE = {
    True: "Last chunk of opened stream",
    False: "Partial"
}
